#include <windows.h>
#include <stdio.h>
#include <string.h>
#include <winnt.h>                     // Only if you call ODBG2_Pluginmainloop

#include "plugin.h"

#define PLUGINNAME     L"HelloWould"    // Unique plugin name
#define VERSION        L"1.0"      // Plugin version

HINSTANCE        hdllinst;             // Instance of plugin DLL

BOOL WINAPI DllEntryPoint(HINSTANCE hi,DWORD reason,LPVOID reserved)
{
    if (reason==DLL_PROCESS_ATTACH)
        hdllinst=hi;                       // Mark plugin instance
    return 1;                            // Report success
};

extc int __cdecl ODBG2_Pluginquery(int ollydbgversion,ulong *features,
                                   wchar_t pluginname[SHORTNAME],wchar_t pluginversion[SHORTNAME])
{
    // Check whether OllyDbg has compatible version. This plugin uses only the
    // most basic functions, so this check is done pro forma, just to remind of
    // this option.
    if (ollydbgversion<201)
        return 0;
    // Report name and version to OllyDbg.
    wcscpy(pluginname,PLUGINNAME);       // Name of plugin
    wcscpy(pluginversion,VERSION);       // Version of plugin
    return PLUGIN_VERSION;               // Expected API version
};

// Optional entry, called immediately after ODBG2_Pluginquery(). Plugin should
// make one-time initializations and allocate resources. On error, it must
// clean up and return -1. On success, it must return 0.
extc int __cdecl ODBG2_Plugininit(void)
{
    // Report success.
    return 0;
}


extc int __cdecl ODBG2_Pluginclose(void)
{
    // For automatical restoring of open windows, mark in .ini file whether
    // Bookmarks window is still open.
    return 0;
};

extc void __cdecl ODBG2_Plugindestroy(void)
{

};

static int ShowHelloWould(t_table *pt,wchar_t *name,ulong index,int mode)
{
    if (mode==MENU_VERIFY)
        return MENU_NORMAL;
    else if (mode==MENU_EXECUTE)
    {
        Resumeallthreads();
        MessageBox(NULL, TEXT("Hello Would"), TEXT("Error"), MB_ICONERROR | MB_OK);
        Suspendallthreads();
        return MENU_NOREDRAW;
    };
    return MENU_ABSENT;
}

static t_menu mainmenu[] =
{
    {
        L"Show Hello would",
        L"Show Hello would message",
        K_NONE, ShowHelloWould, NULL, 0
    },
    { NULL, NULL, K_NONE, NULL, NULL, 0 }//without this falls
};

extc t_menu * __cdecl ODBG2_Pluginmenu(wchar_t *type)
{
    if (wcscmp(type,PWM_MAIN)==0)
    {
        return mainmenu;
    }
    return NULL;                         // No menu
};
